# lkml-scraper
A simple python tool to download mails from the (unofficial) linux kernel
mailing list archive (https://lkml.org/) and save them in maildir format.

## Development status
Work in Progress

## Dependencies
* Arch:
```
# pacman -S python-beautifulsoup4 python-lxml python-requests
```

* Ubuntu/Debian based systems:
```
# apt install python3 python3-bs4 python3-lxml python3-requests
```

* Fedora/Red Hat based systems:
```
# dnf install python3 python3-beautifulsoup4 python3-lxml python3-requests
```

## Usage
```
usage: lkml-scraper.py [-h] --start-date START_DATE --end-date END_DATE
                       [--first-mail-id FIRST_MAIL_ID]
                       maildir

positional arguments:
  maildir               Path of the maildir-directory where mails should be
                        saved

optional arguments:
  -h, --help            show this help message and exit
  --start-date START_DATE
                        Download mails of this day and later. Must be in ISO
                        6801 format (ie. "2017-01-20"). Must not be in the
                        future.
  --end-date END_DATE   Download mails until this day (inclusive). Must be in
                        ISO 6801 format (ie. "2017-01-20"). Must not be in the
                        future. Must not be earlier than '--start-date' Note:
                        If '--start-date' and '--end-date' are the same, all
                        mails of the given date are downloaded. Note: The tool
                        will NOT check if any mails were already downloaded by
                        an earlier execution of the tool. It is up to the user
                        to ensure to avoid such duplicate downloads.
  --first-mail-id FIRST_MAIL_ID
                        First mail id which will be downloaded from day
                        specified by '--start-date'. Must be larger than 0. If
                        it is an invalid number (ie. larger than the highest
                        id for that day) it will be silently ignored and no
                        mails for that day will be downloaded. Beginning from
                        the day afterwards, all mails for each day will be
                        downloaded. This option is mainly intended for
                        continuing previous downloads which were interrupted.
```

## License
lkml-scraper is licensed under the [MIT license](./LICENSE).

## Usage note:
When using lkml-scraper, **please** be nice to lkml.org and don't generate too
much traffic. By default the script waits aproximately 1.5 seconds between each
download. If you need to fetch a large amount of mails just let the program run
several days instead of decreasing the wait time.

All other lkml.org-users will thank you!
